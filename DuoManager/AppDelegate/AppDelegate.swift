//
//  AppDelegate.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen on 1/19/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import UIKit
import SVProgressHUD
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        configLoadingView()
        IQKeyboardManager.shared.enable = true
        return true
    }

    func configLoadingView() {
        SVProgressHUD.setDefaultMaskType(.clear)
    }

}
