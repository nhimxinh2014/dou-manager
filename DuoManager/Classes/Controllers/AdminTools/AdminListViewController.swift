//
//  AdminListViewController.swift
//  DuoManager
//
//  Created by nguyen.tuan.hai on 1/22/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import UIKit

class AdminListViewController: BaseViewController {

    @IBOutlet fileprivate weak var tableView: UITableView!

    fileprivate var admins = [Admin]()
    fileprivate var viewModel = AdminListViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        configTableView()
        getAdmins()
    }

    private func configTableView() {
        tableView.registerNibCell(type: AdminListTableViewCell.self)
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    /*
     Call api get list users
     */
    private func getAdmins() {
        showLoading()
        viewModel.getListAdmins()
            .subscribe(onNext: { [weak self] admins in
                guard let `self` = self else { return }
                self.admins = admins
                self.tableView.reloadData()
                }, onError: { error in
                    showMessage(message: error.message)
            }, onDisposed: {
                dismissLoading()
            }).disposed(by: disposeBag)
    }
}

extension AdminListViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return admins.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableNibCell(type: AdminListTableViewCell.self) else {
            return UITableViewCell()
        }
        let admin = admins[indexPath.row]
        cell.display(admin: admin)
        return cell
    }
}
