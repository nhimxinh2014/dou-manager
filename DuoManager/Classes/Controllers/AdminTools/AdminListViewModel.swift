//
//  AdminListViewModel.swift
//  DuoManager
//
//  Created by nguyen.tuan.hai on 1/22/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import Foundation
import RxSwift

struct AdminListViewModel {

    func getListAdmins() -> Observable<[Admin]> {
        return APIService.shared.getAdmins(GetAdminsInput()).map({ $0.response ?? [] })
    }

}
