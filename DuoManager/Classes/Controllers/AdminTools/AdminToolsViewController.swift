//
//  AdminToolsViewController.swift
//  DuoManager
//
//  Created by nguyen.tuan.hai on 1/22/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import UIKit

class AdminToolsViewController: BaseViewController {

    @IBAction func createAdminAction(_ sender: UIButton) {
        showCreateAdminViewController()
    }

    @IBAction func removeAdminAction(_ sender: UIButton) {
        showAdminList()
    }

    @IBAction func changeNumberPhoneAction(_ sender: UIButton) {
        showAdminList()
    }

    @IBAction func resetLockedOutAdminAction(_ sender: UIButton) {
        showAdminList()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}

extension AdminToolsViewController {
    fileprivate func showAdminList() {
        let adminListVC = AdminListViewController.getVC()
        navigationController?.pushViewController(adminListVC, animated: true)
    }

    fileprivate func showCreateAdminViewController() {
        let adminListVC = CreateAdministratorViewController.getVC()
    navigationController?.pushViewController(adminListVC, animated: true)
    }
}
