//
//  AdminListTableViewCell.swift
//  DuoManager
//
//  Created by nguyen.tuan.hai on 1/22/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import UIKit

class AdminListTableViewCell: UITableViewCell {
    @IBOutlet fileprivate weak var emailLabel: UILabel!
    @IBOutlet fileprivate weak var nameLabel: UILabel!
    @IBOutlet fileprivate weak var phoneLabel: UILabel!
    @IBOutlet fileprivate weak var roleLabel: UILabel!

    fileprivate var admin: Admin!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func display(admin: Admin) {
        self.admin = admin
        emailLabel.text = admin.email
        nameLabel.text = admin.name
        phoneLabel.text = admin.phone
        roleLabel.text = admin.role
    }

}
