//
//  CreateAdministratorViewController.swift
//  DuoManager
//
//  Created by nguyen.tuan.hai on 1/22/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CreateAdministratorViewController: BaseViewController {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var nameTextFiled: UITextField!
    @IBOutlet weak var phoneTextFiled: UITextField!

    @IBAction func requestCreateButtonTapped(_ sender: UIButton) {
        if validateCreateAdministrator() {
            getAdmins()
        }
    }

    let viewModel = CreateAdministratorViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpBindingViewModel()
    }

}

extension CreateAdministratorViewController {
    fileprivate func setUpBindingViewModel() {
        emailTextField.rx.text.map {$0 ?? ""}.bind(to: viewModel.email).disposed(by: disposeBag)
        passwordTextField.rx.text.map {$0 ?? ""}.bind(to: viewModel.password).disposed(by: disposeBag)
        nameTextFiled.rx.text.map {$0 ?? ""}.bind(to: viewModel.name).disposed(by: disposeBag)
        phoneTextFiled.rx.text.map {$0 ?? ""}.bind(to: viewModel.phone).disposed(by: disposeBag)
    }

    fileprivate func validateCreateAdministrator() -> Bool {
        if viewModel.name.value.isEmpty,
            viewModel.phone.value.isEmpty,
            viewModel.email.value.isEmpty,
            viewModel.password.value.isEmpty {
            showMessage(message: "Please enter full infomation")
            return false
        }
        return true
    }

    /*
     Call api get list users
     */
    private func getAdmins() {
        showLoading()
        viewModel.createListAdmins()
            .subscribe(onNext: { admin in
                showMessage(message: "Successed", btnOK: "OK", okCompletion: { [weak self] in
                    self?.navigationController?.popViewController(animated: true)
                })
                }, onError: { error in
                    showMessage(message: error.message)
            }, onDisposed: {
                dismissLoading()
            }).disposed(by: disposeBag)
    }

}
