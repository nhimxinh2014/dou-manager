//
//  CreateAdministratorViewModel.swift
//  DuoManager
//
//  Created by nguyen.tuan.hai on 1/22/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import Foundation
import RxSwift

struct CreateAdministratorViewModel {

    var password = Variable<String>("")
    var email = Variable<String>("")
    var phone = Variable<String>("")
    var name = Variable<String>("")
    var role = RoleAdmins.owner

    func createListAdmins() -> Observable<Admin?> {
        let createaAdministratorInPut = CreateAdministratorInPut.init(email: email.value,
                                                                      password: password.value,
                                                                      name: name.value,
                                                                      phone: phone.value,
                                                                      role: role)
        return APIService.shared.createAdministrator(createaAdministratorInPut).map({ ($0.response )})
    }
}


