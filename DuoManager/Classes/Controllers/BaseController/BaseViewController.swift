//
//  BaseViewController.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen on 1/19/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import UIKit
import RxSwift

class BaseViewController: UIViewController {

    class func getVC() -> Self {
        return getVC(className: String(describing: self))
    }

    class func getVC<T: UIViewController>(className: String) -> T {
        return UIStoryboard.main.instantiateViewController(withIdentifier: className) as! T
    }

    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

