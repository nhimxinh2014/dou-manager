//
//  UserListTableViewCell.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen on 1/21/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import UIKit

protocol UserListTableViewCellDelegate: class {
    func didSelectActionButton(user: User)
}

class UserListTableViewCell: UITableViewCell {

    @IBOutlet fileprivate weak var usernameLabel: UILabel!
    @IBOutlet fileprivate weak var realnameLabel: UILabel!
    @IBOutlet fileprivate weak var emailLabel: UILabel!

    @IBOutlet weak var actionButton: UIButton!

    fileprivate var user: User!
    fileprivate weak var delegate: UserListTableViewCellDelegate?

    func display(user: User, delegate: UserListTableViewCellDelegate) {
        self.user = user
        self.delegate = delegate
        usernameLabel.text = user.username
        realnameLabel.text = user.realname
        emailLabel.text = user.email
    }

    func displayActionButton(title: String, isEnable: Bool) {
        actionButton.setTitle(title, for: .normal)
        actionButton.isEnabled = isEnable
    }

    @IBAction func buttonActionTapped(_ sender: Any) {
        delegate?.didSelectActionButton(user: user)
    }

}
