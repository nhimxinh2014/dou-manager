//
//  UserListViewController.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen on 1/21/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import UIKit

class UserListViewController: BaseViewController {

    @IBOutlet fileprivate weak var tableView: UITableView!

    var listUserType: ListUserType = .unlockUser

    fileprivate var users = [User]()
    fileprivate var viewModel = UserListViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = listUserType.title
        configTableView()
        getUsers()
    }

    func configTableView() {
        tableView.registerNibCell(type: UserListTableViewCell.self)
        tableView.delegate = self
        tableView.dataSource = self
    }

    /*
        Call api get list users
     */
    func getUsers() {
        showLoading()
        viewModel.getListUser()
            .subscribe(onNext: { [weak self] users in
                guard let `self` = self else { return }
                self.users = users
                self.tableView.reloadData()
            }, onError: { error in
                showMessage(message: error.message)
            }, onDisposed: {
                dismissLoading()
            }).disposed(by: disposeBag)
    }

}

extension UserListViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableNibCell(type: UserListTableViewCell.self) else {
            return UITableViewCell()
        }
        let user = users[indexPath.row]
        cell.display(user: user, delegate: self)

        let title = listUserType.activeButtonTitle
        var isEnable = true
        switch listUserType {
        case .unlockUser:
            if let status = user.status, status != .active {
                isEnable = true
            } else {
                isEnable = false
            }
        default:
            break
        }

        cell.displayActionButton(title: title, isEnable: isEnable)
        return cell
    }
}

extension UserListViewController: UserListTableViewCellDelegate {

    func didSelectActionButton(user: User) {
        log("did select user: \(user.username ?? user.userId ?? "")")
        weak var weakSelf = self
        switch listUserType {
        case .unlockUser:
            showLoading()
            viewModel.changeUserStatus(user: user, status: .active)
                .subscribe(onNext: { user in
                    showMessage(message: AppString.Message.unlockUserSuccess, btnOK: AppString.Button.ok,
                                okCompletion: {
                        weakSelf?.getUsers()
                    })
                }, onError: { error in
                    showMessage(message: error.message)
                }, onDisposed: {
                    dismissLoading()
                }).disposed(by: disposeBag)
        case .sendActivation:
            let sendActivationBlock: ((String, String) -> Void) = { [weak self] username, email in
                guard let `self` = self else { return }
                showLoading()
                self.viewModel.enrollUser(username: username, email: email)
                    .subscribe(onNext: { output in
                        showMessage(message: "Enroll success: \(output.response.stringValue)")
                    }, onError: { error in
                        showMessage(message: error.message)
                    }, onDisposed: {
                        dismissLoading()
                    }).disposed(by: `self`.disposeBag)
            }

            let alert = UIAlertController(title: AppString.Title.enrollUser,
                                          message: AppString.Message.enterUsernameEmail, preferredStyle: .alert)
            alert.addTextField { (textField) in
                textField.placeholder = AppString.Title.usernamePlaceHolder
                textField.text = user.username.stringValue
            }
            alert.addTextField { (textField) in
                textField.placeholder = AppString.Title.emailPlaceHolder
                textField.text = user.email.stringValue
            }
            alert.addAction(UIAlertAction(title: AppString.Button.ok, style: .default, handler: { [weak alert] (_) in
                guard let alert = alert else { return }
                let userName = alert.textFields![0].text.stringValue
                let email = alert.textFields![1].text.stringValue

                sendActivationBlock(userName, email)
            }))
            alert.addAction(UIAlertAction(title: AppString.Button.cancel, style: .cancel, handler: { _ in
                log("sendActivation cancel")
            }))
            self.present(alert, animated: true, completion: nil)
        case.enrollUser:
            showMessage(message: "Not found api in documents")
        case .changeUserStatus:
            let changeUserStatus: ((UserStatus) -> Void) = { [weak self] userStatus in
                guard let `self` = self else { return }
                showLoading()
                self.viewModel.changeUserStatus(user: user, status: userStatus)
                    .subscribe(onNext: { user in
                        showMessage(message: AppString.Title.changeUserStatus, btnOK: AppString.Button.ok,
                                    okCompletion: {
                            weakSelf?.getUsers()
                        })
                    }, onError: { error in
                        showMessage(message: error.message)
                    }, onDisposed: {
                        dismissLoading()
                    }).disposed(by: self.disposeBag)
            }
            let alert = UIAlertController(title: AppString.Title.changeUserStatus,
                                          message: AppString.Message.chooseUserStatus,
                                          preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: UserStatus.active.rawValue, style: .default, handler: { _ in
                changeUserStatus(.active)
            }))
            alert.addAction(UIAlertAction(title: UserStatus.bypass.rawValue, style: .default, handler: { _ in
                changeUserStatus(.bypass)
            }))
            alert.addAction(UIAlertAction(title: UserStatus.disable.rawValue, style: .default, handler: { _ in
                changeUserStatus(.disable)
            }))
            alert.addAction(UIAlertAction(title: UserStatus.lockedOut.rawValue, style: .default, handler: { _ in
                changeUserStatus(.lockedOut)
            }))
            alert.addAction(UIAlertAction(title: AppString.Button.cancel, style: .cancel, handler: { _ in
                log("changeUserStatus cancel")
            }))
            self.present(alert, animated: true, completion: nil)
        case .recentLogsUser:
            break
        }
    }

}

enum ListUserType {
    case unlockUser
    case sendActivation
    case enrollUser
    case changeUserStatus
    case recentLogsUser

    var title: String {
        switch self {
        case .unlockUser: return "Unlock user"
        case .sendActivation: return "Send activation"
        case .enrollUser: return "Enroll user"
        case .changeUserStatus: return "Change user status"
        case .recentLogsUser: return "Recent logs of user"
        }
    }

    var activeButtonTitle: String {
        switch self {
        case .unlockUser: return "Unlock"
        case .sendActivation: return "Active"
        case .enrollUser: return "Enroll"
        case .changeUserStatus: return "Change status"
        case .recentLogsUser: return "Show logs"
        }
    }

}
