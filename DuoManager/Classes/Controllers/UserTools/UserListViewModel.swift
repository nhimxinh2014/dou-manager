//
//  UserListViewModel.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen on 1/21/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import Foundation
import RxSwift

struct UserListViewModel {

    func getListUser() -> Observable<[User]> {
        return APIService.shared.getUsers(GetUsersInput()).map({ $0.response ?? [] })
    }

    func changeUserStatus(user: User, status: UserStatus) -> Observable<User> {
        return APIService.shared
            .changeUserStatus(ChangeUserStatusInput(userId: user.userId.stringValue, status: status))
        .map({
            if let first = $0.response {
                return first
            } else {
                throw APIError.invalidData(data: $0)
            }
        })
    }

    func enrollUser(username: String, email: String) -> Observable<EnrollUserOutput> {
        return APIService.shared.enrollUser(EnrollUserInput(username: username, email: email))
    }

}
