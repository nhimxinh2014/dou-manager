//
//  UserToolsViewController.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen on 1/21/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import UIKit

class UserToolsViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func unlockUserButtonTapped() {
        showUserListWithType(type: .unlockUser)
    }

    @IBAction func sendActivationButtonTapped() {
        showUserListWithType(type: .sendActivation)
    }

    @IBAction func enrollUserButtonTapped() {
        showUserListWithType(type: .enrollUser)
    }

    @IBAction func changeUserStatusButtonTapped() {
        showUserListWithType(type: .changeUserStatus)
    }

    @IBAction func recentLogsUserButtonTapped() {
        showUserListWithType(type: .recentLogsUser)
    }

}

extension UserToolsViewController {
    fileprivate func showUserListWithType(type: ListUserType) {
        let userListVC = UserListViewController.getVC()
        userListVC.listUserType = type
        navigationController?.pushViewController(userListVC, animated: true)
    }
}
