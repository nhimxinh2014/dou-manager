//
//  ViewController.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen on 1/19/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
   
    @IBAction func showUserToolsButtonTapped(_ sender: UIButton) {
        let userTools = UserToolsViewController.getVC()
        navigationController?.pushViewController(userTools, animated: true)
    }

    @IBAction func showAdminToolsButtonTapped(_ sender: UIButton) {
        let userTools = AdminToolsViewController.getVC()
        navigationController?.pushViewController(userTools, animated: true)
    }

    @IBAction func showLogsViewButtonTapped(_ sender: UIButton) {
        //do something
    }
    
    @IBAction func showSettingButtonTapped(_ sender: UIButton) {
        //do something
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }

}

