//
//  APIBase.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen on 1/19/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
import Alamofire

enum ResponseStatus: String {
    case ok = "OK"
    case fail = "FAIL"
}

class APIInputBase {

    let path: String
    let requestType: HTTPMethod
    let parameters: [String: Any]?

    init(path: String, parameters: [String: Any]?, requestType: HTTPMethod) {
        self.path = path
        self.parameters = parameters
        self.requestType = requestType
    }

}

class APIOutputBase: Mappable {

    var stat = ResponseStatus.ok

    init() {
    }

    required init?(map: Map) {
        guard map.JSON["stat"] != nil else {
            return nil
        }
    }

    func mapping(map: Map) {
        stat <- map["stat"]
    }
}

class APIOutputObject<T>: APIOutputBase where T: Mappable {

    var response: T?

    override func mapping(map: Map) {
        super.mapping(map: map)
        response <- map["response"]
    }

    required init?(map: Map) {
        super.init(map: map)
        mapping(map: map)
    }
}

class APIOutputObjects<T>: APIOutputBase where T: Mappable {

    var response: [T]?

    override func mapping(map: Map) {
        super.mapping(map: map)
        response <- map["response"]
    }

    required init?(map: Map) {
        super.init(map: map)
        mapping(map: map)
    }
}


class APIOutputFail: APIOutputBase, Error {

    var code = 0
    var message: String?
    var messageDetail: String?

    override func mapping(map: Map) {
        super.mapping(map: map)
        code <- map["code"]
        message <- map["message"]
        messageDetail <- map["message_detail"]
    }

    required init?(map: Map) {
        super.init(map: map)
        mapping(map: map)
    }
}

struct APIError: Error {

    static let invalidDataCode = 601
    static let noCode = 602

    var data: Any?
    var code = 0
    var message: String?

    static func jsonPraseError(data: Any?) -> APIError {
        return APIError(data: data, code: 3840, message: "Server error. Please try again later.")
    }

    static func invalidData(data: Any?) -> APIError {
        return APIError(data: data, code: invalidDataCode, message: "Invalid data format")
    }

}

extension Error {
    var message: String {
        if let error = self as? APIError {
            return error.message ?? ""
        }
        if let error = self as? APIOutputFail {
            let errorMessageDetail = error.messageDetail ?? ""
            return "\(error.message ?? ""): " + errorMessageDetail
        }
        return localizedDescription
    }
}
