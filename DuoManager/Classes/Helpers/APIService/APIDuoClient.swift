//
//  Client.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen on 1/19/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import Foundation

open class APIDuoClient: NSObject {
    let ikey: String
    let skey: String
    let host: String

    required public init(ikey: String,
                         skey: String,
                         host: String) {
        self.ikey = ikey
        self.skey = skey
        self.host = host
    }
    
    fileprivate func makeRequest(_ method: String,
                       uri: String,
                       headers: Dictionary<String, String>,
                       body: String,
                       completion: @escaping APIResult) -> URLSessionDataTask {
        let config = URLSessionConfiguration.default
        config.httpAdditionalHeaders = headers
        let session = URLSession(
            configuration: config,
            delegate: nil,
            delegateQueue: nil)
        let url = URL(string: "https://\(self.host)\(uri)")
        var request = URLRequest.init(url: url!)
        request.httpMethod = method
        if body != "" {
            request.httpBody = body.data(using: String.Encoding.utf8)
        }
        let task = session.dataTask(with: request, completionHandler: completion)
        task.resume()
        return task
    }
    
    /*
        params should either be Dictionary<String, String> or Dictionary<String, [String]>.
     */
    func duoAPICall(_ method: String, path: String, params: Dictionary<String, AnyObject>, completion: @escaping APIResult) -> URLSessionDataTask {
        let now = Util.rfc2822Date(Date())
        let normalizedParams: Dictionary<String, [String]> = Util.normalizeParams(params)
        let authHeader = Util.basicAuth(self.ikey,
                                        skey: self.skey,
                                        method: method,
                                        host: self.host,
                                        path: path,
                                        dateString: now,
                                        params: normalizedParams)
        let canonicalizedParams: String = Util.canonicalizeParams(normalizedParams)
        var body: String = ""
        var uri: String = ""
        var headers = [
            "Authorization": authHeader,
            "Date": now,
            "Host": self.host
        ]
        if ["POST", "PUT"].contains(method) {
            headers["Content-Type"] = "application/x-www-form-urlencoded"
            body = canonicalizedParams
            uri = path
        } else {
            uri = canonicalizedParams != "" ? "\(path)?\(canonicalizedParams)" : path
        }
        
        // Do the request.
        return self.makeRequest(method, uri: uri, headers: headers, body: body, completion: completion)
    }

}
