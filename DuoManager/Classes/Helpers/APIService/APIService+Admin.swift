//
//  APIService+Admin.swift
//  DuoManager
//
//  Created by nguyen.tuan.hai on 1/22/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import Foundation
import ObjectMapper
import RxSwift

enum RoleAdmins: String {
    case owner = "Owner"
    case administrator = "Administrator"
    case applicationManager = "Application Manager"
    case userManager = "User Manager"
    case helpDesk = "Help Desk"
    case billing = "Billing"
}

extension APIService {

    func getAdmins(_ input: GetAdminsInput) -> Observable<APIOutputObjects<Admin>> {
        return request(input)
    }

    func createAdministrator(_ input: CreateAdministratorInPut) -> Observable<APIOutputObject<Admin>> {
        return request(input)
    }

}

class CreateAdministratorInPut: APIInputBase {
    init(email: String, password: String, name: String, phone: String, role: RoleAdmins) {
        let param = [
            "email": email,
            "password": password,
            "name": name,
            "phone": phone,
            "role": role.rawValue
        ]
        super.init(path: APIUrls.admins, parameters: param, requestType: .post)
    }
}

class GetAdminsInput: APIInputBase {
    init(offset: Int = 0) {
        let param = [
            "offset": offset
        ]
        super.init(path: APIUrls.admins, parameters: param, requestType: .get)
    }
}
