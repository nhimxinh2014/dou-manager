//
//  APIService+Users.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen on 1/19/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import Foundation
import ObjectMapper
import RxSwift

extension APIService {

    func getUsers(_ input: GetUsersInput) -> Observable<APIOutputObjects<User>> {
        return request(input)
    }

    func changeUserStatus(_ input: ChangeUserStatusInput) -> Observable<APIOutputObject<User>> {
        return request(input)
    }

    func enrollUser(_ input: EnrollUserInput) -> Observable<EnrollUserOutput> {
        return request(input)
    }

//    func getRecentLogsOfUser(_ input: GetRecentLogsOfUserInput) -> Observable<> {
//
//    }
}

class GetUsersInput: APIInputBase {
    init(offset: Int = 0) {
        let param = [
            "offset": offset
        ]
        super.init(path: APIUrls.users, parameters: param, requestType: .get)
    }
}

class ChangeUserStatusInput: APIInputBase {
    init(userId: String, status: UserStatus) {
        let path = APIUrls.users + "/\(userId)"
        let param = [
            "status": status.rawValue
        ]
        super.init(path: path, parameters: param, requestType: .post)
    }
}

class EnrollUserInput: APIInputBase {
    init(username: String, email: String) {
        let params = [
            "username": username,
            "email": email
        ]
        super.init(path: APIUrls.enrollUsers, parameters: params, requestType: .post)
    }
}

class EnrollUserOutput: APIOutputBase {
    var response: String?

    override func mapping(map: Map) {
        super.mapping(map: map)
        response <- map["response"]
    }

    required init?(map: Map) {
        super.init(map: map)
        mapping(map: map)
    }
}

class GetRecentLogsOfUserInput: APIInputBase {
    init(userId: String) {
        let path = APIUrls.authenticationLogs
        let params = [
            "limit": 10,
            "users": userId,
            "mintime": 0,
            "maxtime": Date().timeIntervalSince1970 * 1000
            ] as [String : Any]
        super.init(path: path, parameters: params, requestType: .get)
    }
}
