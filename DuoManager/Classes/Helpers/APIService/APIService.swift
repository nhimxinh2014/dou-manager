//
//  APIService.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen on 1/19/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import RxSwift
import ObjectMapper

typealias JSONDictionary = [String: Any]
typealias APIResult = (Data?, URLResponse?, Error?) -> Void

class APIService {

    static let shared = APIService()
    fileprivate let client: APIDuoClient

    private init() {
        client = APIDuoClient(ikey: integrationKey, skey: secretKey, host: APIUrls.host)
    }

}

extension APIService {

    func request<T: Mappable>(_ input: APIInputBase) -> Observable<T> {
        return request(input)
            .map { json -> T in
                if let t = T(JSON: json) {
                    return t
                }
                throw APIError.invalidData(data: json)
            }
            .observeOn(MainScheduler.instance)
            .share(replay: 1)
    }

    // MARK: - Support functions: convert request to Observable
    fileprivate func request(_ input: APIInputBase) -> Observable<JSONDictionary> {

        log("=========================================")
        log("Path: \(input.path)")
        log("Params: \(input.parameters ?? [:])")
        log("=========================================")

        return Observable.create { observer in
            let task = self.client.duoAPICall(input.requestType.rawValue, path: input.path,
                params: (input.parameters ?? [:]) as Dictionary<String, AnyObject>,
                completion: { dataResponse, _, error in
                    do {
                        let json = try self.process(dataResponse, error: error)
                        observer.onNext(json)
                        observer.onCompleted()
                    } catch {
                        observer.onError(error)
                    }
            })
            return Disposables.create {
                task.cancel()
            }
        }
    }

    // Handle success or faild of response
    fileprivate func process(_ dataResponse: Data?, error: Error?) throws -> JSONDictionary {
        if let error = error {
            throw error
        }
        guard let data = dataResponse else {
            throw APIError.jsonPraseError(data: dataResponse)
        }
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as AnyObject
            log("Response: \(json)")
            if let dic = json as? [String: Any], let output = APIOutputBase(JSON: dic) {
                if output.stat == .fail, let errorOutput = APIOutputFail(JSON: dic) {
                    throw errorOutput
                }
                return dic
            } else {
                throw APIError.jsonPraseError(data: json)
            }
        } catch {
            if error is APIOutputFail {
                throw error
            } else {
                throw APIError.jsonPraseError(data: data)
            }
        }
    }

}
