//
//  APIUrls.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen on 1/19/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import UIKit

struct APIUrls {

    static let host = "api-3c05b394.duosecurity.com"
    static let apiVersion = "/admin/v1/"

}

extension APIUrls {
    static let users = apiVersion + "users"
    static let enrollUsers = apiVersion + "users/enroll"
    static let authenticationLogs = "/admin/v2/logs/authentication"
    static let admins = apiVersion + "admins"
}
