//
//  CommonHelper.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen on 1/19/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import Foundation
import SVProgressHUD

func showLoading() {
    SVProgressHUD.show()
}

func dismissLoading() {
    SVProgressHUD.dismiss()
}
