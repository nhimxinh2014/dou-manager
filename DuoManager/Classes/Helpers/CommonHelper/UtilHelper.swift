//
//  Util.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen on 1/19/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import Foundation
import SwiftyJSON

class Util: NSObject {
    /*
        Return a date string that conforms to the format described in RFC 2822.
     
        https://www.ietf.org/rfc/rfc2822.txt
     */
    class func rfc2822Date(_ date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss Z"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        let dateString = dateFormatter.string(from: date)
    
        return dateString
    }
    
    /*
        Return copy of params with string listified and unicode strings utf-8 encoded.
        params should either be Dictionary<String, String> or Dictionary<String, [String]>.
     */
    class func normalizeParams(_ params: Dictionary<String, Any>) -> Dictionary<String, [String]> {
        var params = params
        var result: Dictionary<String, [String]> = [:]

        // TODO: Have to double check
        for (key, values) in params {
            // normalize values to String arrays
            if let values = JSON(values).string {
                params[key] = [values]
            }
            
            // utf8 encode the values
            var values: [String] = []
            if let aValues = params[key] as? [Any] {
                for value in aValues {
                    values.append(JSON(value).stringValue.toUTF8())
                }
            }
            result[key.toUTF8()] = values
        }
        return result
    }
    
    /*
        Return a canonical string version of the given request parameters.
     */
    class func canonicalizeParams(_ params: Dictionary<String, [String]>) -> String {
        var firstOneAdded = false
        var paramsAsString: String = ""
        let contentKeys: Array<String> = Array(params.keys).sorted(by: {
            $0.compare($1, options: NSString.CompareOptions.literal) == ComparisonResult.orderedAscending
        })
        
        for contentKey in contentKeys {
            let contentValues: Array<String> = params[contentKey]!
            let sortedValues = contentValues.sorted(by: {
                $0.compare($1, options: NSString.CompareOptions.literal) == ComparisonResult.orderedAscending
            })
            
            for value in sortedValues {
                if (!firstOneAdded) {
                    paramsAsString += contentKey.stringByAddingPercentEncodingForRFC3986()! + "=" + (value.stringByAddingPercentEncodingForRFC3986())!
                    firstOneAdded = true
                } else {
                    paramsAsString += "&" + contentKey.stringByAddingPercentEncodingForRFC3986()! + "=" + (value.stringByAddingPercentEncodingForRFC3986())!
                }
            }
        }
        return paramsAsString
    }
    
    /*
        Return signature version 2 canonical string of given request attributes.
     */
    class func canonicalize(_ method: String,
                              host: String,
                              path: String,
                              params: Dictionary<String, [String]>,
                              dateString: String) -> String {
        let canonicalHeaders = [
            dateString, method.uppercased(),
            host.lowercased(), path,
            self.canonicalizeParams(params)]
        return canonicalHeaders.joined(separator: "\n")
    }
    
    /*
        Return basic authorization header line with a Duo Web API signature.
     */
    class func basicAuth(_ ikey: String,
                           skey: String,
                           method: String,
                           host: String,
                           path: String,
                           dateString: String,
                           params: Dictionary<String, [String]>) -> String {
        // Create the canonical string.
        let canonicalHeadersString = canonicalize(method, host: host, path: path, params: params, dateString: dateString)
        
        // Sign the canonical string.
        let signatureHexDigest = canonicalHeadersString.hmac(CryptoAlgorithm.sha1, key: skey)
        let authHeader = "\(ikey):\(signatureHexDigest)"
        let base64EncodedAuthHeader = authHeader.toBase64Encode()
        return "Basic \(base64EncodedAuthHeader)"
    }

    class func getPresentableViewController(by viewController: UIViewController?) -> UIViewController? {
        let viewControllerReturn: UIViewController?
        if let viewController = viewController {
            viewControllerReturn = getVisibleViewController(from: viewController)
        } else {
            viewControllerReturn = getVisibleViewControllerOfWindow()
        }
        return viewControllerReturn
    }

    /*
        Return Visible View Controller Of Window
     */
    class func getVisibleViewControllerOfWindow() -> UIViewController? {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        guard let viewController = appDelegate?.window?.rootViewController else {
            return nil
        }
        return getVisibleViewController(from: viewController)
    }

    class func getVisibleViewController(from viewController: UIViewController?) -> UIViewController? {
        if let navigationViewController = viewController as? UINavigationController {
            return getVisibleViewController(from: navigationViewController.visibleViewController)
        } else if let tabbarViewController = viewController as? UITabBarController {
            return getVisibleViewController(from: tabbarViewController.selectedViewController)
        } else {
            if let presentedViewController = viewController?.presentedViewController {
                return getVisibleViewController(from: presentedViewController)
            } else {
                return viewController
            }
        }
    }

}


func showMessage(message: String, btnOK: String = "OK", okCompletion: (() -> Void)? = nil) {
    let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: btnOK, style: UIAlertActionStyle.default, handler: { _ in
        okCompletion?()
    }))
    Util.getVisibleViewControllerOfWindow()?.present(alert, animated: true, completion: nil)
}
