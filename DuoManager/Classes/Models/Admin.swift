//
//  Admin.swift
//  DuoManager
//
//  Created by nguyen.tuan.hai on 1/22/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import Foundation
import ObjectMapper

class Admin: Mappable {

    var adminId: String?
    var email: String?
    var lastLogin = 0
    var name: String?
    var phone: String?
    var restrictedByAdminUnits = false
    var role: String?

    func mapping(map: Map) {
        adminId <- map["admin_id"]
        email <- map["email"]
        lastLogin <- map["last_login"]
        name <- map["name"]
        phone <- map["phone"]
        restrictedByAdminUnits <- map["restricted_by_admin_units"]
        role <- map["role"]
    }

    required init?(map: Map) {
        mapping(map: map)
    }

}
