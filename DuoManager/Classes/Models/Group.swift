//
//  Group.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen Nguyen Ngoc on 1/21/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import Foundation
import ObjectMapper

class Group: Mappable {

    var desc: String?
    var name: String?

    func mapping(map: Map) {
        desc <- map["desc"]
        name <- map["name"]
    }

    required init?(map: Map) {
        mapping(map: map)
    }

}
