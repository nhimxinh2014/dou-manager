//
//  Phone.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen Nguyen Ngoc on 1/21/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import Foundation
import ObjectMapper

class Phone: Mappable {

    var activated = true
    var capabilities: [String]?
    var extensionS: String?
    var lastSeen: String?
    var name: String?
    var number: String?
    var phoneId: String?
    var platform: String?
    var postdelay: String?
    var predelay: String?
    var smsPasscodesSent = false
    var type: String?

    func mapping(map: Map) {
        activated <- map["activated"]
        capabilities <- map["capabilities"]
        extensionS <- map["extension"]
        lastSeen <- map["last_seen"]
        name <- map["name"]
        number <- map["number"]
        phoneId <- map["phone_id"]
        platform <- map["platform"]
        postdelay <- map["postdelay"]
        predelay <- map["predelay"]
        smsPasscodesSent <- map["sms_passcodes_sent"]
        type <- map["type"]
    }

    required init?(map: Map) {
        mapping(map: map)
    }

}
