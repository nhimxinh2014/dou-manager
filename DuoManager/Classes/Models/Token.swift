//
//  Token.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen Nguyen Ngoc on 1/21/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import Foundation
import ObjectMapper

class Token: Mappable {

    var serial: String?
    var tokenId: String?
    var type: String?

    func mapping(map: Map) {
        serial <- map["serial"]
        tokenId <- map["token_id"]
        type <- map["type"]
    }

    required init?(map: Map) {
        mapping(map: map)
    }

}
