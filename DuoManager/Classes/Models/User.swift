//
//  User.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen Nguyen Ngoc on 1/21/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import Foundation
import ObjectMapper

class User: Mappable {

    var alias1: String?
    var alias2: String?
    var alias3: String?
    var alias4: String?
    var created = 0
    var desktoptokens: [String]? // unknown
    var email: String?
    var firstname: String?
    var groups: [Group]?
    var lastDirectorySync: String?
    var lastLogin: String?
    var lastname: String?
    var notes: String?
    var phones: [Phone]?
    var realname: String?
    var status: UserStatus?
    var tokens: [Token]?
    var u2ftokens: [String]? // unknown
    var userId: String?
    var username: String?

    func mapping(map: Map) {
        alias1 <- map["alias1"]
        alias2 <- map["alias2"]
        alias3 <- map["alias3"]
        alias4 <- map["alias4"]
        created <- map["created"]
        desktoptokens <- map["desktoptokens"]
        email <- map["email"]
        firstname <- map["firstname"]
        groups <- map["groups"]
        lastDirectorySync <- map["last_directory_sync"]
        lastLogin <- map["last_login"]
        lastname <- map["lastname"]
        notes <- map["notes"]
        phones <- map["phones"]
        realname <- map["realname"]
        status <- map["status"]
        tokens <- map["tokens"]
        u2ftokens <- map["u2ftokens"]
        userId <- map["user_id"]
        username <- map["username"]
    }

    required init?(map: Map) {
        mapping(map: map)
    }

}

enum UserStatus: String {
    case active = "active"
    case bypass = "bypass"
    case disable = "disable"
    case lockedOut = "locked out"
}
