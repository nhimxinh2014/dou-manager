//
//  Message.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen Nguyen Ngoc on 1/22/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import Foundation

struct AppString {

    struct Message {
        static let unlockUserSuccess = "Unlock user success"
        static let enrollUserSuccess = "Enroll user success"
        static let enterUsernameEmail = "Enter username and email to enroll"
        static let chooseUserStatus = "Select user status"
        static let changeUserStatusSuccess = "Change user status success"
    }

    struct Button {
        static let ok = "OK"
        static let cancel = "Cancel"
    }

    struct Title {
        static let enrollUser = "Enroll User"
        static let usernamePlaceHolder = "Enter username"
        static let emailPlaceHolder = "Enter email"
        static let changeUserStatus = "Change user status"
    }

}
