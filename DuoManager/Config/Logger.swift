//
//  Logger.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen on 1/19/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import Foundation

func log(_ data: Any, fun: String = #function, line: Int = #line) {
    // TODO: not print in production mode, show function and line code if need
    print("\(data)")
}
