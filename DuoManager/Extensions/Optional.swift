//
//  String.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen on 1/21/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import Foundation

extension Optional where Wrapped==String {

    var stringValue: String {
        return self ?? ""
    }

}
