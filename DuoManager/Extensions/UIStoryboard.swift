//
//  Extension.swift
//  DuoManager
//
//  Created by Tuan Hai Nguyen on 1/21/19.
//  Copyright © 2019 Duo. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    static let main = UIStoryboard(name: "Main", bundle: .main)
}
