//
//  UIView.swift
//  FFSeller
//
//  Created by Tuan Hai Nguyen Nguyen Ngoc on 12/22/17.
//  Copyright © 2017 boostapp. All rights reserved.
//

import Foundation
import UIKit

extension UIView {

    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }

    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }

    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }

    @discardableResult func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        guard let nibView = nib.instantiate(withOwner: self, options: nil).first as? UIView else {
            return nil
        }
        addSubview(nibView)
        nibView.boundsToSuperView()
        return nibView
    }

    func boundsToSuperView() {
        if let superView = self.superview {
            frame = superView.bounds
            autoresizingMask = [.flexibleWidth, .flexibleHeight]
            translatesAutoresizingMaskIntoConstraints = true
        }
    }

    func getSubview<T: UIView>(ofType type: T.Type) -> [T] {
        var listSubView = [T]()
        func getSub(_ view: UIView) {
            for item in view.subviews {
                if let input = item as? T {
                    listSubView.append(input)
                } else {
                    getSub(item)
                }
            }
        }
        getSub(self)
        return listSubView
    }

    func getSuperView<T: UIView>(ofType type: T.Type) -> T? {
        var superView = self.superview
        while !(superView is T) {
            superView = superView?.superview
            if superView == nil {
                return nil
            }
        }
        return superView as? T
    }

    func disableTouchFast() {
        isUserInteractionEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.isUserInteractionEnabled = true
        }
    }

    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }

}

extension UIBarButtonItem {
    func disableTouchFast() {
        isEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.isEnabled = true
        }
    }
}

extension UIView {

    func radius(radius: CGFloat = 3.0) {
        layer.cornerRadius = radius
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.clear.cgColor
        layer.masksToBounds = true
    }

    func shadow(radius: CGFloat = 3.0) {
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = radius
        layer.shadowOpacity = 0.7
        layer.masksToBounds = false
        //layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 3.0).cgPath
    }
}

extension UICollectionViewCell {

    func makeShadow() {
        contentView.radius()
        shadow()
    }

}
